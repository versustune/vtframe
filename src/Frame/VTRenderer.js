class VTRenderer {
    constructor(VTCanvas) {
        this.canvas = VTCanvas;
    }

    start() {
        let context = this.canvas.context;
        context.beginPath();
        return context;
    }

    end(context) {
        this.canvas.draw();
        context.closePath();
    }

    ellipse(x, y, r1, r2) {
        let context = this.start();
        context.ellipse(x, y, r1, r2, 0, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    circle(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    point(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        context.fill();
        context.closePath();
        return this;
    }

    rect(x, y, w, h) {
        let context = this.start();
        context.rect(x, y, w, h);
        this.end(context);
        return this;
    }

    fromTo(x1, y1, x2, y2) {
        let context = this.start();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        this.end(context);
        return this;
    }

    fromToCurved(x1, y1, x2, y2) {
        let context = this.start();
        let x_mid = (x1 + x2) / 2;
        let y_mid = (y1 + y2) / 2;
        let cp_x1 = (x_mid + x1) / 2;
        let cp_x2 = (x_mid + x2) / 2;
        context.moveTo(x1, y1);
        context.quadraticCurveTo(cp_x1, y1, x_mid, y_mid);
        context.quadraticCurveTo(cp_x2, y2, x2, y2);
        this.end(context);
        return this;
    }

    image(image, x, y, w, h) {
        if (!image) {
            return;
        }
        w = w || image.width;
        h = h || image.height;
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.drawImage(image, x, y, w, h);
        this.end(context);
        return this;
    }

    imageSprite(frame, posX, posY, sizeW, sizeH) {
        if (!sprite instanceof VTSprite) {
            return;
        }
        sizeW = sizeW || sprite.w;
        sizeH = sizeH || sprite.h;
        posX = posX || 0;
        posY = posY || 0;
        let context = this.start();
        context.drawImage(frame.image, frame.item.x, frame.item.y, frame.w, frame.h, posX, posY, sizeW, sizeH);
        this.end(context);
        return this;
    }

    translate(x, y) {
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.translate(x, y);
        return this;
    }

    resetTranslate() {
        let context = this.start();
        context.setTransform(1, 0, 0, 1, 0, 0);
        this.end(context);
        return this;
    }

    setFont(size, fontFamily) {
        size = size || 16;
        fontFamily = fontFamily || 'Arial';
        let context = this.canvas.context;
        context.font = size + 'px' + ' ' + fontFamily;
        return this;
    }

    //text utils
    text(string, x, y) {
        string = string || '';
        x = x || 0;
        y = y || 0;

        let context = this.start();
        context.fillText(string, x, y);
        return this;
    }

    save() {
        this.canvas.context.save();
        return this;
    }

    restore() {
        this.canvas.context.restore();
        return this;
    }
}