class VTEngine {
    constructor(frame, config) {
        this.frame = frame;
        this.config = config;
        this.blLoop = true;
        return this;
    }

    initListener() {
        document.addEventListener('keypress', this.config.keyPressed.bind(this, event));
        document.addEventListener('keydown', this.config.keyDown.bind(this, event));
        document.addEventListener('keyup', this.config.keyUp.bind(this, event));
        document.addEventListener('mousemove', this.config.mouseMove.bind(this, event));
        document.addEventListener('mousedown', this.config.mouseDown.bind(this, event));
        document.addEventListener('mouseup', this.config.mouseUp.bind(this, event));
    }

    start() {
        this.initListener();
        this.config.setup.call(this.frame);
        this.loop();
        console.timeEnd("VTFrame_" + this.frame.id);
    }

    loop() {
        if (this.blLoop) {
            requestAnimationFrame(this.loop.bind(this));
        }
        this.config.draw.call(this.frame);
    }

    loopMode(boolean) {
        this.blLoop = boolean;
        if (boolean) {
            this.loop();
        }
    }

    fps() {
        if (!this.lastRun) {
            this.lastRun = new Date().getTime();
            return 0;
        }
        let delta = (new Date().getTime() - this.lastRun) / 1000;
        this.lastRun = new Date().getTime();
        return Math.floor(1 / delta);
    }
}