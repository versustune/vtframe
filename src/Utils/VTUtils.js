class VTUtils {
    static random(min, max) {
        let rand = Math.random();
        if (typeof min === 'undefined') {
            return rand;
        } else if (typeof max === 'undefined') {
            if (min instanceof Array) {
                return min[Math.floor(rand * min.length)];
            } else {
                return rand * min;
            }
        } else {
            if (min > max) {
                let tmp = min;
                min = max;
                max = tmp;
            }
            return rand * (max - min) + min;
        }
    };

    static randomInt(min, max) {
        return Math.floor(VTUtils.random(min, max));
    }

    static normalize(val, max, min) {
        return (val - min) / (max - min);
    };

    static getColor(a, b, c, alpha, mode, useAlpha) {
        if (mode === 'hex') {
            a = a || '#FFF';
            return a;
        }
        if (a && b === undefined) {
            return a;
        }
        a = a || '0';
        b = b || '0';
        c = c || '0';
        alpha = alpha || '1';

        let color = a + ',' + b + ',' + c;
        if (useAlpha) {
            color += ',' + alpha;
        }
        return `${mode}(${color})`;
    }

    static distance(x, y, x2, y2) {
        let a = x - x2;
        let b = y - y2;

        return Math.sqrt(a * a + b * b);
    }

    static map(n, start1, stop1, start2, stop2, withinBounds) {
        let newVal = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newVal;
        }
        if (start2 < stop2) {
            return this.constrain(newVal, start2, stop2);
        } else {
            return this.constrain(newVal, stop2, start2);
        }
    };

    static constrain(n, low, high) {
        return Math.max(Math.min(n, high), low);
    }

    static reverseNumber(num, min, max) {
        return (max + min) - num;
    }
}