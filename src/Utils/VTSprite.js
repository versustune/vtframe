class VTSprite {

    /**
     * this need a resource from the VTLoader
     * @param resource
     * @param w
     * @param h
     * @param json
     */
    constructor(resource, w, h, json) {
        this.resource = resource;
        this.w = w;
        this.h = h;
        this.map = json ? JSON.parse(json) : this.create();
        this.position = 0;
    }

    create() {
        let object = this.resource.object,
            width = object.width,
            height = object.height,
            map = [],
            count = 0;

        for (let y = 0; y < height; y += this.h) {
            for (let x = 0; x < width; x += this.w) {
                map.push({x: x, y: y, name: 'sprite_' + count});
                count++;
            }
        }

        this.map = map;
    }

    getByName(name) {
        let lastPosition = this.position;
        for (let i = 0; i < this.map.length; i++) {
            let entry = this.map[i];
            if (entry.name === name) {
                this.position = i;
                break;
            }
        }
        let frame = this.getFrame();
        this.position = lastPosition;
        return frame;
    }

    getFrame() {
        return {
            image: this.resource.object,
            item: this.map[this.position],
            w: this.w,
            h: this.h
        }
    }

    nextFrame() {
        if (this.position < this.map.length - 1) {
            this.position++;
        } else {
            this.position = 0;
        }
    }

    previousFrame() {
        if (this.position > 0) {
            this.position--;
        } else {
            this.position = this.map.length - 1;
        }
    }

}