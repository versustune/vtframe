//return an object with to max velocity
function findMaxVelocity(h) {
    let value = 8.5;
    let scale = 1 + (h / 1000);
    return value * scale;
}

function getColor() {
    return {
        r: VTUtils.random(255),
        g: VTUtils.random(255),
        b: VTUtils.random(255),
        a: 255
    }
}

function lifeSpan(isParticle) {
    let value = isParticle ? VTUtils.random(125, 255) : 255;
    let max = isParticle ? 8 : 2;
    let min = isParticle ? 2 : 0;
    return {
        value: value, //alpha it :D
        weight: VTUtils.random(1, 5),
        ttl: VTUtils.random(min, max),
        pCount: VTUtils.random(1, 150),
        startTick: Math.floor(VTUtils.random(1, 10)) //let the firework start later..
    }
}

function getScale(h) {
    let scale = 1;
    scale = h < 800 ? 0.5 : scale;
    scale = h > 1500 ? 1.5 : scale;
    return scale;
}

function explodeConfig() {
    let tmp = {
        isMulti: VTUtils.random() < 0.5,
        colors: [],
        isRainbow: VTUtils.random() < 0.02
    };

    let ran = 1;
    if (tmp.isMulti) {
        ran = Math.floor(VTUtils.random(2, 5));
    }
    for (let i = 0; i < ran; i++) {
        tmp.colors.push(getColor());
    }

    return tmp;
}

function createParticle(pos, config, frame) {
    let color = config.colors[Math.floor(VTUtils.random(config.colors.length))];
    if (config.isRainbow) {
        color = null;
    }
    return new Particle(pos, color, frame)
}