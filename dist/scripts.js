class VTNoise {
    constructor(seed) {
        seed = seed || Math.random();
        this.seed = VTUtils.random(-seed, seed);

        this.lastValue = 0;
    }

    static noise(value, seed) {
        seed = seed || Math.random();
        seed = VTUtils.random(-seed, seed);
        return value + seed;
    }

    logicNoise(value) {
        let randomValue;
        if (this.lastValue < 0) {
            randomValue = VTUtils.random(this.seed);
        } else {
            randomValue = VTUtils.random(-this.seed, 0);
        }

        this.lastValue = randomValue;
        return value + randomValue;
    }

}
class VTSprite {

    /**
     * this need a resource from the VTLoader
     * @param resource
     * @param w
     * @param h
     * @param json
     */
    constructor(resource, w, h, json) {
        this.resource = resource;
        this.w = w;
        this.h = h;
        this.map = json ? JSON.parse(json) : this.create();
        this.position = 0;
    }

    create() {
        let object = this.resource.object,
            width = object.width,
            height = object.height,
            map = [],
            count = 0;

        for (let y = 0; y < height; y += this.h) {
            for (let x = 0; x < width; x += this.w) {
                map.push({x: x, y: y, name: 'sprite_' + count});
                count++;
            }
        }

        this.map = map;
    }

    getByName(name) {
        let lastPosition = this.position;
        for (let i = 0; i < this.map.length; i++) {
            let entry = this.map[i];
            if (entry.name === name) {
                this.position = i;
                break;
            }
        }
        let frame = this.getFrame();
        this.position = lastPosition;
        return frame;
    }

    getFrame() {
        return {
            image: this.resource.object,
            item: this.map[this.position],
            w: this.w,
            h: this.h
        }
    }

    nextFrame() {
        if (this.position < this.map.length - 1) {
            this.position++;
        } else {
            this.position = 0;
        }
    }

    previousFrame() {
        if (this.position > 0) {
            this.position--;
        } else {
            this.position = this.map.length - 1;
        }
    }

}
class VTUtils {
    static random(min, max) {
        let rand = Math.random();
        if (typeof min === 'undefined') {
            return rand;
        } else if (typeof max === 'undefined') {
            if (min instanceof Array) {
                return min[Math.floor(rand * min.length)];
            } else {
                return rand * min;
            }
        } else {
            if (min > max) {
                let tmp = min;
                min = max;
                max = tmp;
            }
            return rand * (max - min) + min;
        }
    };

    static randomInt(min, max) {
        return Math.floor(VTUtils.random(min, max));
    }

    static normalize(val, max, min) {
        return (val - min) / (max - min);
    };

    static getColor(a, b, c, alpha, mode, useAlpha) {
        if (mode === 'hex') {
            a = a || '#FFF';
            return a;
        }
        if (a && b === undefined) {
            return a;
        }
        a = a || '0';
        b = b || '0';
        c = c || '0';
        alpha = alpha || '1';

        let color = a + ',' + b + ',' + c;
        if (useAlpha) {
            color += ',' + alpha;
        }
        return `${mode}(${color})`;
    }

    static distance(x, y, x2, y2) {
        let a = x - x2;
        let b = y - y2;

        return Math.sqrt(a * a + b * b);
    }

    static map(n, start1, stop1, start2, stop2, withinBounds) {
        let newVal = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newVal;
        }
        if (start2 < stop2) {
            return this.constrain(newVal, start2, stop2);
        } else {
            return this.constrain(newVal, stop2, start2);
        }
    };

    static constrain(n, low, high) {
        return Math.max(Math.min(n, high), low);
    }

    static reverseNumber(num, min, max) {
        return (max + min) - num;
    }
}
class VTVector {
    constructor(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    //helper
    static createRandom(x, y, z) {
        x = x || 1;
        y = y || 1;
        z = z || 0;
        return new VTVector(VTUtils.random(-x, x), VTUtils.random(-y, y), VTUtils.random(-z, z));
    }

    mult(times) {
        this.x *= times;
        this.y *= times;
        this.z *= times;
    }

    set(vector) {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
    }

    add(vector) {
        this.x = this.x + vector.x;
        this.y = this.y + vector.y;
        this.z = this.z + vector.z;
    }

    addXYZ(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
    }

    clone() {
        return new VTVector(this.x, this.y, this.z);
    }
}
class VTEngine {
    constructor(frame, config) {
        this.frame = frame;
        this.config = config;
        this.blLoop = true;
        return this;
    }

    initListener() {
        document.addEventListener('keypress', this.config.keyPressed.bind(this, event));
        document.addEventListener('keydown', this.config.keyDown.bind(this, event));
        document.addEventListener('keyup', this.config.keyUp.bind(this, event));
        document.addEventListener('mousemove', this.config.mouseMove.bind(this, event));
        document.addEventListener('mousedown', this.config.mouseDown.bind(this, event));
        document.addEventListener('mouseup', this.config.mouseUp.bind(this, event));
    }

    start() {
        this.initListener();
        this.config.setup.call(this.frame);
        this.loop();
        console.timeEnd("VTFrame_" + this.frame.id);
    }

    loop() {
        if (this.blLoop) {
            requestAnimationFrame(this.loop.bind(this));
        }
        this.config.draw.call(this.frame);
    }

    loopMode(boolean) {
        this.blLoop = boolean;
        if (boolean) {
            this.loop();
        }
    }

    fps() {
        if (!this.lastRun) {
            this.lastRun = new Date().getTime();
            return 0;
        }
        let delta = (new Date().getTime() - this.lastRun) / 1000;
        this.lastRun = new Date().getTime();
        return Math.floor(1 / delta);
    }
}
window._vtfc = 0;

class VTFrame {
    constructor(config) {
        this.id = window._vtfc++;
        console.time("VTFrame_" + this.id);
        config = VTFrame.getDefaultConfig(config);
        this.config = config;
        this.style = new VTStyle(VTFrame.createCanvas(config.width, config.height, config), config);
        this.renderer = new VTRenderer(this.style, config);
        this.engine = new VTEngine(this, config);
        this.assets = new VTLoader();
        this.eventSet = false;
        return this;
    }

    static createCanvas(width, height, config) {
        width = width || 800;
        height = height || 600;
        let c = document.createElement('canvas');
        c.height = height;
        c.width = width;
        if (!config['appendTo'] instanceof Node) {
            config['appendTo'] = document.body;
        }
        config['appendTo'].appendChild(c);
        return c;
    }

    static getDefaultConfig(config) {
        this.getDefault('mode', '2d', config);
        this.getDefault('height', innerHeight, config);
        this.getDefault('width', innerWidth, config);
        this.getDefault('appendTo', document.body, config);
        this.getDefault('draw', 'f', config);
        this.getDefault('setup', 'f', config);
        this.getDefault('preload', 'f', config);
        this.getDefault('keyPressed', 'f', config);
        this.getDefault('keyDown', 'f', config);
        this.getDefault('keyUp', 'f', config);
        this.getDefault('mouseDown', 'f', config);
        this.getDefault('mouseUp', 'f', config);
        this.getDefault('mouseMove', 'f', config);
        return config;
    }

    static getDefault(parameter, callback, config) {
        if (callback === 'f') {
            callback = function () {
            };
        }
        config[parameter] = config[parameter] || callback;
    }

    run() {
        this.config.preload.call(this);
        if (this.assets.preloading()) {
            if (!this.eventSet) {
                window.addEventListener(VTLOADER_EVENT, this.engine.start.bind(this.engine));
                this.eventSet = true;
            }
        } else {
            this.engine.start.call(this.engine);
        }
    }

    getHeight() {
        return this.config.height;
    }

    getWidth() {
        return this.config.width;
    }

}

let VTLOADER_AUDIO = 'audio';
let VTLOADER_IMAGE = 'image';
let VTLOADER_EVENT = 'VTLoader_Preload';

class VTLoader {
    constructor() {
        this.resources = {};
        this.count = 0;
        this.preload = {
            count: 0,
            finished: 0
        }
    }

    static errored(event) {
        console.error(event);
    }

    load(url, type, name, preload, event) {
        event = event || 'load';
        name = name || 'VTR' + this.count++;
        console.time(name);
        preload = preload === true;

        if (this.resources.hasOwnProperty(name)) {
            console.error(`Resource with the name: ${name} already exists`);
            return;
        }

        let resource;
        if (type === 'audio')
            resource = new Audio();
        else if (type === 'image')
            resource = new Image();
        else {
            console.error(`Type: ${type} is not a valid type`);
            return;
        }


        this.resources[name] = {
            object: resource,
            name: name,
            preload: preload,
            loaded: false
        };

        if (preload) {
            this.preload.count++;
        }

        resource.addEventListener(event, this.success.bind(this));
        resource.onerror = VTLoader.errored;
        resource._vtname = name;
        resource.src = url;
    }

    success(event) {
        let target = event.target;
        let resource = this.resources[target._vtname];
        console.timeEnd(target._vtname);
        resource.loaded = true;
        if (resource.preload) {
            this.preload.finished++;
            if (!this.preloading()) {
                console.log("VTLoader", "Preloading finished");
                dispatchEvent(new CustomEvent(VTLOADER_EVENT, {bubbles: true}))
            }
        }
    }

    getResource(name) {
        return this.resources[name];
    }

    preloading() {
        let stillLoading = false;
        let keys = Object.keys(this.resources);
        for (let key of keys) {
            let resource = this.resources[key];
            if (resource.preload && !resource.loaded) {
                stillLoading = true;
                break;
            }
        }
        return stillLoading;
    }
}
class VTLoadingScreen {


}
class VTRenderer {
    constructor(VTCanvas) {
        this.canvas = VTCanvas;
    }

    start() {
        let context = this.canvas.context;
        context.beginPath();
        return context;
    }

    end(context) {
        this.canvas.draw();
        context.closePath();
    }

    ellipse(x, y, r1, r2) {
        let context = this.start();
        context.ellipse(x, y, r1, r2, 0, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    circle(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    point(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        context.fill();
        context.closePath();
        return this;
    }

    rect(x, y, w, h) {
        let context = this.start();
        context.rect(x, y, w, h);
        this.end(context);
        return this;
    }

    fromTo(x1, y1, x2, y2) {
        let context = this.start();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        this.end(context);
        return this;
    }

    fromToCurved(x1, y1, x2, y2) {
        let context = this.start();
        let x_mid = (x1 + x2) / 2;
        let y_mid = (y1 + y2) / 2;
        let cp_x1 = (x_mid + x1) / 2;
        let cp_x2 = (x_mid + x2) / 2;
        context.moveTo(x1, y1);
        context.quadraticCurveTo(cp_x1, y1, x_mid, y_mid);
        context.quadraticCurveTo(cp_x2, y2, x2, y2);
        this.end(context);
        return this;
    }

    image(image, x, y, w, h) {
        if (!image) {
            return;
        }
        w = w || image.width;
        h = h || image.height;
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.drawImage(image, x, y, w, h);
        this.end(context);
        return this;
    }

    imageSprite(frame, posX, posY, sizeW, sizeH) {
        if (!sprite instanceof VTSprite) {
            return;
        }
        sizeW = sizeW || sprite.w;
        sizeH = sizeH || sprite.h;
        posX = posX || 0;
        posY = posY || 0;
        let context = this.start();
        context.drawImage(frame.image, frame.item.x, frame.item.y, frame.w, frame.h, posX, posY, sizeW, sizeH);
        this.end(context);
        return this;
    }

    translate(x, y) {
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.translate(x, y);
        return this;
    }

    resetTranslate() {
        let context = this.start();
        context.setTransform(1, 0, 0, 1, 0, 0);
        this.end(context);
        return this;
    }

    setFont(size, fontFamily) {
        size = size || 16;
        fontFamily = fontFamily || 'Arial';
        let context = this.canvas.context;
        context.font = size + 'px' + ' ' + fontFamily;
        return this;
    }

    //text utils
    text(string, x, y) {
        string = string || '';
        x = x || 0;
        y = y || 0;

        let context = this.start();
        context.fillText(string, x, y);
        return this;
    }

    save() {
        this.canvas.context.save();
        return this;
    }

    restore() {
        this.canvas.context.restore();
        return this;
    }
}
class VTStyle {
    constructor(canvas, config) {
        this.canvas = canvas;
        this.context = canvas.getContext(config.mode);
        this.init();
    }

    init() {
        this.config = {
            stroke: true,
            fill: false,
            colorMode: 'rgb',
            alpha: false
        }
    }

    colorMode(mode) {
        mode = mode || 'rgb';
        this.config.colorMode = mode;
        return this;
    }

    fillColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.fillStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    strokeColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.strokeStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    alpha(boolean) {
        this.config.alpha = boolean;
        return this;
    }

    strokeWeight(weight) {
        this.context.lineWidth = weight;
        return this;
    }

    stroke(boolean) {
        this.config.stroke = boolean;
        return this;
    }

    fill(boolean) {
        this.config.fill = boolean;
        return this;
    }

    draw() {
        let context = this.context;
        let config = this.config;
        if (config.stroke)
            context.stroke();
        if (config.fill)
            context.fill();
        return this;
    }

    background() {
        let context = this.context;
        context.beginPath();
        context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    clear() {
        let context = this.context;
        context.beginPath();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    getContext() {
        return this.context;
    }
}