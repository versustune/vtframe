const gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-terser'),
    srcPath = __dirname + '/../src/**/*.js',
    distPath = __dirname + '/../dist';

gulp.task('default', function () {
    return gulp.src(srcPath)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(distPath))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(distPath))
});

gulp.task('watch', ['default'], function () {
    gulp.watch(srcPath, ['default']);
});